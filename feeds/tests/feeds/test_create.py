from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from rest_framework.reverse import reverse
from rest_framework.views import status

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('feed-list')
        self.client.force_login(self.super_admin)

    @classmethod
    def setUpTestData(cls):
        cls.super_admin = User.objects.create_superuser(username='admin',
                                                        password='1234')

    def tearDown(self):
        self.client.logout()

    def test_new(self):
        data = {
            'source': 'http://cnn.com/news2.xml',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class Perm(APITestCase):

    def setUp(self):
        self.url = reverse('feed-list')

    @classmethod
    def setUpTestData(cls):
        cls.superuser = User.objects.create_superuser(username='admin', password='1234')
        cls.admin1 = User.objects.create_user(username='admin1', password='1234')
        cls.admin2 = User.objects.create_user(username='admin2',
                                              password='1234')
        cls.admin2.user_permissions.add(Permission.objects.get(codename='add_feed'))

    def test_unauthenticated(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_admin1(self):
        self.client.force_login(self.admin1)
        response = self.client.post(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin2(self):
        self.client.force_login(self.admin2)
        response = self.client.post(self.url, data={'source': 'http://cnn.com'})
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
