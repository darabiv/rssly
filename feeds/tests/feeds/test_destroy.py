import json
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from rest_framework.reverse import reverse
from rest_framework.views import status

from django_celery_beat.models import PeriodicTask, IntervalSchedule

from feeds.models import Feed

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('feed-detail', kwargs={'pk': self.feed.pk})
        self.client.force_login(self.super_admin)

    @classmethod
    def setUpTestData(cls):
        cls.super_admin = User.objects.create_superuser(username='admin',
                                                        password='1234')
        cls.feed = Feed.objects.create(title='my feed')
        interval, created = IntervalSchedule.objects.get_or_create(
            every=30,
            period='seconds')
        pt = PeriodicTask.objects.create(
            name=f'Updating feed {cls.feed.pk}',
            task='feeds.tasks.update_feed',
            interval=interval,
            args=json.dumps([cls.feed.pk]))
        cls.feed.pt = pt
        cls.feed.save()

    def tearDown(self):
        self.client.logout()

    def test_ok(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class Perm(APITestCase):

    def setUp(self):
        self.url = reverse('feed-detail', kwargs={'pk': self.feed.pk})

    @classmethod
    def setUpTestData(cls):
        cls.superuser = User.objects.create_superuser(username='admin', password='1234')
        cls.admin1 = User.objects.create_user(username='admin1', password='1234')
        cls.admin2 = User.objects.create_user(username='admin2',
                                              password='1234')
        cls.admin2.user_permissions.add(Permission.objects.get(codename='delete_feed'))
        cls.feed = Feed.objects.create(title='my feed')

    def test_unauthenticated(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_admin1(self):
        self.client.force_login(self.admin1)
        response = self.client.delete(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin2(self):
        self.client.force_login(self.admin2)
        response = self.client.delete(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)