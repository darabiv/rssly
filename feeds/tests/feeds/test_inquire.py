from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from rest_framework.views import status


User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('feed-inquire-feed-registry')
        self.client.force_login(self.super_admin)

    @classmethod
    def setUpTestData(cls):
        cls.super_admin = User.objects.create_superuser(username='admin',
                                                        password='1234')

    def tearDown(self):
        self.client.logout()

    def test_inquire(self):
        data = {
            'task_id': 'my_task_id',
        }
        response = self.client.patch(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['state'], 'PENDING')
        self.assertEqual(response.data['result'], None)


class Perm(APITestCase):

    def setUp(self):
        self.url = reverse('feed-inquire-feed-registry')

    @classmethod
    def setUpTestData(cls):
        cls.admin1 = User.objects.create_user(username='admin1', password='1234')

    def test_unauthenticated(self):
        response = self.client.patch(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_admin1(self):
        self.client.force_login(self.admin1)
        data = {
            'task_id': 'my_task_id',
        }
        response = self.client.patch(self.url, data=data)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
