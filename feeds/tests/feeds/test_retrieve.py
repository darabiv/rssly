from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from rest_framework.reverse import reverse
from rest_framework.views import status


from feeds.models import Feed

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('feed-detail', kwargs={'pk': self.feed.pk})
        self.client.force_login(self.super_admin)

    @classmethod
    def setUpTestData(cls):
        cls.super_admin = User.objects.create_superuser(username='admin',
                                                        password='1234')
        cls.feed = Feed.objects.create(title='my feed')

    def tearDown(self):
        self.client.logout()

    def test_ok(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class Perm(APITestCase):

    def setUp(self):
        self.url = reverse('feed-detail', kwargs={'pk': self.feed.pk})

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username='user1', password='1234')
        cls.feed = Feed.objects.create(title='my feed')

    def test_unauthenticated(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_authenticated(self):
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_200_OK)