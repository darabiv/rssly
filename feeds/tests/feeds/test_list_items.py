from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from rest_framework.views import status


from feeds.models import Feed, FeedItem

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('feed-list-items', kwargs={'pk': self.feed1.pk})
        self.client.force_login(self.super_admin)

    @classmethod
    def setUpTestData(cls):
        cls.super_admin = User.objects.create_superuser(username='admin',
                                                        password='1234')
        cls.feed1 = Feed.objects.create(source='http://a.com', title='my feed1')
        cls.feed2 = Feed.objects.create(source='http://b.com', title='my feed2')
        feed_items = [
            FeedItem(feed=cls.feed1, title='item 1', guid='1'),
            FeedItem(feed=cls.feed1, title='item 2', guid='2'),
            FeedItem(feed=cls.feed2, title='item 1', guid='1'),
        ]
        FeedItem.objects.bulk_create(feed_items)

    def tearDown(self):
        self.client.logout()

    def test_ok(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)


class Perm(APITestCase):

    def setUp(self):
        self.url = reverse('feed-list-items', kwargs={'pk': self.feed.pk})

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username='user1', password='1234')
        cls.feed = Feed.objects.create(title='my feed')

    def test_unauthenticated(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user(self):
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_200_OK)