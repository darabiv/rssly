from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from rest_framework.reverse import reverse
from rest_framework.views import status

from feeds.models import Feed

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('feed-list')
        self.client.force_login(self.super_admin)

    @classmethod
    def setUpTestData(cls):
        cls.super_admin = User.objects.create_superuser(username='admin',
                                                        password='1234')
        feeds = [
            Feed(source='http://a.com', title='Feed 1'),
            Feed(source='http://b.com', title='Feed 2'),
            Feed(source='http://c.com', title='Feed 3'),
        ]
        Feed.objects.bulk_create(feeds)

    def tearDown(self):
        self.client.logout()

    def test_all(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class Perm(APITestCase):

    def setUp(self):
        self.url = reverse('feed-list')

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username='user1', password='1234')

    def test_unauthenticated(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user(self):
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
