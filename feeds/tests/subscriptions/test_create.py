from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from rest_framework.views import status

from feeds.models import Feed, Subscription

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('subscription-list')
        self.client.force_login(self.super_admin)

    @classmethod
    def setUpTestData(cls):
        cls.super_admin = User.objects.create_superuser(username='admin',
                                                        password='1234')
        cls.feed = Feed.objects.create(title='my feed')

    def tearDown(self):
        pass
        # self.client.logout()

    def test_new(self):
        data = {
            'feed': self.feed.pk,
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.super_admin.subscriptions.count(), 1)

    def test_duplicate(self):
        Subscription.objects.create(user=self.super_admin, feed=self.feed)
        data = {
            'feed': self.feed.pk,
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class Perm(APITestCase):

    def setUp(self):
        self.url = reverse('subscription-list')

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username='user1', password='1234')
        cls.feed = Feed.objects.create(title='my feed')

    def test_unauthenticated(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user(self):
        self.client.force_login(self.user)
        response = self.client.post(self.url, data={'feed': self.feed.pk})
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
