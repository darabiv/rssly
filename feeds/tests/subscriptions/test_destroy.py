from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from rest_framework.views import status

from feeds.models import Feed, Subscription

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('subscription-detail',
                           kwargs={'pk': self.subscription.pk})
        self.client.force_login(self.super_admin)

    @classmethod
    def setUpTestData(cls):
        cls.super_admin = User.objects.create_superuser(username='admin',
                                                        password='1234')
        cls.feed = Feed.objects.create(title='my feed')
        cls.subscription = Subscription.objects.create(user=cls.super_admin,
                                                       feed=cls.feed)

    def tearDown(self):
        self.client.logout()

    def test_ok(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class Perm(APITestCase):

    def setUp(self):
        self.url = reverse('subscription-detail', kwargs={'pk': self.subscription.pk})

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username='user1')
        cls.other_user = User.objects.create_user(username='user2')
        cls.feed = Feed.objects.create(title='my feed')
        cls.subscription = Subscription.objects.create(user=cls.user,
                                                       feed=cls.feed)

    def test_unauthenticated(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_owner(self):
        self.client.force_login(self.user)
        response = self.client.delete(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_unauthorized(self):
        self.client.force_login(self.other_user)
        response = self.client.delete(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
