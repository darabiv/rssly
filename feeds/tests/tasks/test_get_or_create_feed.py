from unittest.mock import patch
from django.test import TestCase

import requests
from django_celery_beat.models import PeriodicTask

from feeds.models import Feed
from feeds.tasks import get_or_create_feed
from feeds.parsers import DefaultParseResult


class GetOrCreateFeedTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.my_feed = Feed.objects.create(source='http://cnn.com')

    @patch(
        'feeds.parsers.DefaultParserBackend.parse',
        return_value=DefaultParseResult(
            {
                'title': 'My Feed'
            },
            [
                {
                    'title': 'New Entry',
                    'link': 'https://cnn.com/entry1.html'
                }
            ]
        )
    )
    def test_new_feed(self, result):
        feed_link = 'https://www.entekhab.ir/fa/rss/1'
        result = get_or_create_feed(feed_link)
        self.assertTrue(Feed.objects.filter(source=feed_link).exists())
        new_feed = Feed.objects.get(source=feed_link)
        self.assertTrue(isinstance(new_feed.pt, PeriodicTask))
        self.assertEqual(result.get('source'), feed_link)

    def test_existing_feed(self):
        existing_feed = Feed.objects.first()
        result = get_or_create_feed(existing_feed.source)
        self.assertEqual(result.get('source'), existing_feed.source)

    def test_connection_error(self):
        feed_link = 'https://www.entekhb.ir/fa/rss/1'
        self.assertRaises(requests.exceptions.ConnectionError, get_or_create_feed, feed_link)
