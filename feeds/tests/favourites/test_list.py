from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from rest_framework.reverse import reverse
from rest_framework.views import status

from feeds.models import Feed, FeedItem, Favourite

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('favourite-list')
        self.client.force_login(self.super_admin)

    @classmethod
    def setUpTestData(cls):
        cls.feed = Feed.objects.create(source='http://a.com', title='Feed 1')
        cls.feed_item1 = FeedItem.objects.create(feed=cls.feed, guid='1')
        cls.feed_item2 = FeedItem.objects.create(feed=cls.feed, guid='2')
        cls.super_admin = User.objects.create_superuser(username='superadmin')
        cls.user1 = User.objects.create_user(username='user1')
        cls.user2 = User.objects.create_user(username='user2')
        favourites = [
            Favourite(user=cls.user1, feed_item=cls.feed_item1),
            Favourite(user=cls.user2, feed_item=cls.feed_item2)
        ]
        Favourite.objects.bulk_create(favourites)

    def tearDown(self):
        self.client.logout()

    def test_all(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)


class Perm(APITestCase):

    def setUp(self):
        self.url = reverse('favourite-list')

    @classmethod
    def setUpTestData(cls):
        cls.feed = Feed.objects.create(source='http://a.com', title='Feed 1')
        cls.feed_item1 = FeedItem.objects.create(feed=cls.feed, guid='1')
        cls.feed_item2 = FeedItem.objects.create(feed=cls.feed, guid='2')
        cls.super_admin = User.objects.create_superuser(username='superadmin')
        cls.user1 = User.objects.create_user(username='user1')
        cls.user2 = User.objects.create_user(username='user2')
        cls.user2.user_permissions.add(
            Permission.objects.get(codename='view_favourite')
        )
        favourites = [
            Favourite(user=cls.user1, feed_item=cls.feed_item1),
            Favourite(user=cls.user2, feed_item=cls.feed_item2)
        ]
        Favourite.objects.bulk_create(favourites)

    def test_unauthenticated(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_view_perm(self):
        self.client.force_login(self.user2)
        response = self.client.get(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

    def test_user(self):
        self.client.force_login(self.user1)
        response = self.client.get(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
