from django.db import IntegrityError
from rest_framework import serializers
from rest_framework.serializers import ValidationError

from feeds.models import Feed, Subscription, Favourite, FeedItem


class FeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feed
        fields = '__all__'


class FeedItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeedItem
        fields = '__all__'


class FeedRegisterSerializer(serializers.Serializer):
    source = serializers.URLField()


class SubscriptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subscription
        fields = '__all__'
        read_only_fields = ('user',)

    def create(self, validated_data):
        try:
            instance = Subscription.objects.create(**validated_data)
            return instance
        except IntegrityError:
            raise ValidationError('Object already exists')


class FavouriteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Favourite
        fields = '__all__'
        read_only_fields = ('user',)

    def create(self, validated_data):
        try:
            instance = Favourite.objects.create(**validated_data)
            return instance
        except IntegrityError:
            raise ValidationError('Object already exists')
