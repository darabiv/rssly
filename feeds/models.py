from django.db import models, transaction
from django.contrib.auth import get_user_model

from django_celery_beat.models import PeriodicTask


class Feed(models.Model):
    source = models.URLField(unique=True, db_index=True)
    title = models.CharField(max_length=1024, blank=True)
    updated_at = models.DateTimeField(null=True)
    published_at = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    pt = models.ForeignKey(PeriodicTask, related_name='feed',
                           on_delete=models.SET_NULL, null=True)

    def update(self, result):
        with transaction.atomic():
            self.title = result.feed['title']
            self.save()
            feed_items = []
            for entry in result.entries:
                feed_items.append(
                    FeedItem(
                        feed=self,
                        title=entry['title'],
                        link=entry['link'],
                        summary=entry.get('summary', ''),
                        guid=entry.get('id') or entry['link']
                    )
                )
            FeedItem.objects.bulk_create(feed_items, ignore_conflicts=True)

    class Meta:
        default_permissions = ['add', 'view', 'delete']


class FeedItem(models.Model):
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE,
                             related_name='items', editable=False)
    fetched_at = models.DateTimeField(auto_now_add=True)
    guid = models.CharField(max_length=1024, editable=False)
    title = models.CharField(max_length=2048, editable=False)
    link = models.URLField(editable=False)
    summary = models.TextField(max_length=10000)
    updated_at = models.DateTimeField(null=True)
    published_at = models.DateTimeField(null=True)

    class Meta:
        unique_together = ('feed', 'guid')
        default_permissions = []


class Subscription(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,
                             related_name='subscriptions')
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE,
                             related_name='subscriptions')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'feed')
        default_permissions = ['view']


class Favourite(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,
                             related_name='favourites')
    feed_item = models.ForeignKey(FeedItem, on_delete=models.CASCADE,
                                  related_name='favourites')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'feed_item')
        default_permissions = ['view']
