import io

import requests
import feedparser

from feeds.exceptions import InvalidFeedUrl


class DefaultParseResult:

    def __init__(self, feed, entries):
        self._feed = feed
        self._entries = entries

    @property
    def feed(self):
        return self._feed

    @property
    def entries(self):
        return self._entries


class DefaultParserBackend:
    parse_result_class = DefaultParseResult

    def get_parse_result_class(self):
        assert self.parse_result_class is not None, (
                "'%s' should either include a `parse_result_class` attribute, "
                "or override the `get_parse_result_class()` method."
                % self.__class__.__name__
        )

        return self.parse_result_class

    def parse(self, source):
        resp = requests.get(source, timeout=20.0)
        content = io.BytesIO(resp.content)
        result = feedparser.parse(content)

        if not result.entries:
            raise InvalidFeedUrl(f'Invalid feed source: {source}')

        parse_result_class = self.get_parse_result_class()

        return parse_result_class(result.feed, result.entries)
