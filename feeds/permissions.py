from rest_framework.permissions import BasePermission
from rest_framework.permissions import IsAuthenticated


class FeedPermissions(BasePermission):
    def has_permission(self, request, view):
        if view.action == 'create':
            return request.user.has_perm('feeds.add_feed') or request.user.is_superuser

        return IsAuthenticated().has_permission(request, view)

    def has_object_permission(self, request, view, obj):

        if request.user.is_superuser:
            return True

        if view.action in {'retrieve', 'list_items'}:
            return IsAuthenticated().has_object_permission(request, view, obj)

        if view.action == 'destroy':
            return request.user.has_perm('feeds.delete_feed')

        return False


class SubscriptionPermissions(BasePermission):
    def has_permission(self, request, view):
        return IsAuthenticated().has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        return request.user == obj.user
