from django.db import transaction
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet, ReadOnlyModelViewSet
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action

from django_celery_beat.models import PeriodicTask
from celery.result import AsyncResult

from feeds.models import Subscription, Favourite, Feed, FeedItem
from feeds.permissions import FeedPermissions, SubscriptionPermissions
from feeds.serializers import FeedSerializer, FeedRegisterSerializer,\
    SubscriptionSerializer, FavouriteSerializer, FeedItemSerializer
from feeds.tasks import get_or_create_feed


class FeedViewSet(GenericViewSet,
                  mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.CreateModelMixin,
                  mixins.DestroyModelMixin):
    queryset = Feed.objects.order_by('-created_at')
    serializer_class = FeedSerializer
    filter_fields = '__all__'
    ordering_fields = '__all__'
    permission_classes = [FeedPermissions]

    def perform_destroy(self, instance):
        with transaction.atomic():
            if instance.pt:
                PeriodicTask.objects.filter(pk=instance.pt.pk).delete()
            instance.delete()

    @action(methods=['patch'], detail=False,
            serializer_class=FeedRegisterSerializer)
    def register(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        async_result = get_or_create_feed.delay(serializer.data['source'])

        return Response({'task_id': async_result.task_id}, status.HTTP_200_OK)

    @action(methods=['patch'], detail=False, url_path='inquire')
    def inquire_feed_registry(self, request):
        task_id = request.data.get('task_id')
        if not task_id:
            return Response({'task_id': ['This field is required']},
                            status.HTTP_400_BAD_REQUEST)

        async_result = AsyncResult(task_id)
        data = {
            'state': async_result.state,
            'result': async_result.result
        }
        return Response(data, status.HTTP_200_OK)

    @action(methods=['get'], detail=True, url_path='items', serializer_class=FeedItemSerializer)
    def list_items(self, request, pk=None):
        queryset = FeedItem.objects.filter(feed__pk=pk).order_by('-fetched_at')
        queryset = self.filter_queryset(queryset)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class SubscriptionViewSet(GenericViewSet,
                          mixins.ListModelMixin,
                          mixins.CreateModelMixin,
                          mixins.DestroyModelMixin):
    queryset = Subscription.objects.order_by('-created_at').select_related(
        'feed', 'user')
    serializer_class = SubscriptionSerializer
    filter_fields = '__all__'
    ordering_fields = '__all__'
    permission_classes = [SubscriptionPermissions]

    def get_queryset(self):
        qs = super().get_queryset()
        if not (self.request.user.has_perm('feeds.view_subscription') or self.request.user.is_superuser):
            qs = qs.filter(user=self.request.user)

        return qs

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class FavouriteViewSet(GenericViewSet,
                       mixins.ListModelMixin,
                       mixins.CreateModelMixin,
                       mixins.DestroyModelMixin):
    queryset = Favourite.objects.order_by('-created_at').select_related(
        'feed_item', 'user')
    serializer_class = FavouriteSerializer
    filter_fields = '__all__'
    ordering_fields = '__all__'
    permission_classes = [SubscriptionPermissions]

    def get_queryset(self):
        qs = super().get_queryset()
        if not (self.request.user.has_perm(
                'feeds.view_favourite') or self.request.user.is_superuser):
            qs = qs.filter(user=self.request.user)

        return qs

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
