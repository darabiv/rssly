import json
import importlib
from django.conf import settings
from django.db import IntegrityError, transaction
from django.db.models import ObjectDoesNotExist
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from celery import shared_task

from feeds.parsers import DefaultParseResult, DefaultParserBackend


if hasattr(settings, 'FEED_PARSER_BACKEND'):
    splited = settings.FEED_PARSER_BACKEND.split('.')
    _class = splited[-1]
    module = '.'.join(splited[:-1])
    parser_backend_module = importlib.import_module(module)
    parser_backend_class = getattr(parser_backend_module, _class)
    assert issubclass(parser_backend_class, DefaultParserBackend), 'parser backend class must be subclass of "DefaultParserBackend" class'
else:
    parser_backend_class = DefaultParserBackend


@shared_task
def fetch_feed(source):
    backend = parser_backend_class()
    result = backend.parse(source)
    assert isinstance(result, DefaultParseResult)
    return result


@shared_task
def update_feed(feed_id):
    from feeds.models import Feed
    feed = Feed.objects.get(pk=feed_id)
    result = fetch_feed(feed.source)
    feed.update(result)


@shared_task
def get_or_create_feed(source):
    from feeds.models import Feed
    from feeds.serializers import FeedSerializer
    try:
        instance = Feed.objects.get(source=source)
    except ObjectDoesNotExist:
        try:
            result = fetch_feed(source)
            with transaction.atomic():
                instance = Feed.objects.create(source=source)
                instance.update(result)
                interval, created = IntervalSchedule.objects.get_or_create(
                    every=30,
                    period='seconds')
                pt = PeriodicTask.objects.create(
                    name=f'Updating feed {instance.pk}',
                    task='feeds.tasks.update_feed',
                    interval=interval,
                    args=json.dumps([instance.pk]))
                instance.pt = pt
                instance.save()
        except IntegrityError:
            instance = Feed.objects.get(source=source)

    return FeedSerializer(instance).data
