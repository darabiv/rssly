# RSSly


## Installation for Development on Ubuntu

##### Update Ubuntu repositories
```bash
sudo apt update
```

##### Install following packages
```bash
sudo apt install -y python3-venv redis-server libpq-dev postgresql postgresql-contrib
```

##### Configure Postgres Database

```bash
sudo -u postgres psql
postgres=# CREATE DATABASE [db_name];
postgres=# CREATE USER [db_username] WITH PASSWORD 'password';
postgres=# ALTER ROLE [db_username] SET client_encoding TO 'utf8';
postgres=# ALTER ROLE [db_username] SET default_transaction_isolation TO 'read committed';
postgres=# ALTER ROLE [db_username] SET timezone TO 'UTC';
postgres=# GRANT ALL PRIVILEGES ON DATABASE [db_name] TO [db_username];
postgres=# \q
```

##### Clone the repo from develop branch
```bash
git clone git@gitlab.com:darabiv/rssly.git
```

##### Install Pip Packages
In project directory, create the venv, activate it and install the packages from given requirements text file as below:

```bash
python3 -m venv .venv
source .venv/bin/activate
python -m pip install -U pip
pip install -r requirements.txt
```

##### Configure .env file
In project directory, create and open .env file and configure following variables:

```bash
sudo nano /path/to/project/folder/.env
```

```text
DEBUG=True
SECRET_KEY=changeme
ALLOWED_HOSTS=* #comma separated list of allowed ips to host the app or * to allow any
CORS_ORIGIN_WHITELIST= #comma separated list of allowed clients to access the app or * to allow any
CELERY_BROKER_URL=redis://localhost:6379
CELERY_RESULT_BACKEND=redis://localhost:6379
CELERY_TASK_DEFAULT_QUEUE=rssly
DB_NAME=[db name]
DB_USER=[db user]
DB_PASSWORD=[db password]
DB_HOST=localhost
DB_PORT=[empty] or 3306
JWT_ACCESS_TOKEN_LIFETIME=60
JWT_REFRESH_TOKEN_LIFETIME=600

```

##### Make DB migrations and static files collection

```bash
python manage.py makemigrations
python manage.py migrate
python manage.py createcachetable
python manage.py collectstatic --noinput
```

## Run development server

```
python manage.py runserver
```

Now you can browse API at:
```bash
http://[host]:[port]/swagger
```

## Run tests
```
python manage.py test --settings=proj.test_settings
```

## Run celery
```
celery -A proj worker -l info
```

## Run celery beat
```
celery -A proj beat -l INFO --scheduler django_celery_beat.schedulers:DatabaseScheduler
```

## Feed Parser Backends
The default feed parser backend is 'feeds.parsers.DefaultParserBackend'. You can write your own custom backend and  
specify it in settings:

```
FEED_PARSER_BACKEND = 'mymodule.parsers.CustomParserBackend'
```