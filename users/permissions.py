from rest_framework.permissions import BasePermission
from rest_framework.permissions import IsAuthenticated


class UserPermissions(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True

        if view.action in {'retrieve', 'update',
                           'partial_update', 'destroy'}:
            return IsAuthenticated().has_permission(request, view)

        elif view.action in {'list'}:
            return request.user.has_perm('users.view_user')

        elif view.action in {'create'}:
            return True

        return False

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True

        elif view.action == 'retrieve':
            return request.user.has_perm(
                'users.view_user') or request.user == obj

        elif view.action in {'update', 'partial_update'}:
            return request.user.has_perm(
                'users.change_user') or request.user == obj

        return False