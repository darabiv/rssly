from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse
from rest_framework.views import status

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('user-detail', kwargs={'pk': 1})
        self.client.force_login(self.me)

    def tearDown(self):
        self.client.logout()

    @classmethod
    def setUpTestData(cls):
        cls.me = User.objects.create_user(username='admin', password='1234')

    def test_update_me(self):
        update_data = {
            'first_name': 'vahid'
        }
        response = self.client.patch(self.url, data=update_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_password(self):
        update_data = {
            'password': 'Wex!@y1235'
        }
        response = self.client.patch(self.url, data=update_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.me.refresh_from_db()
        self.assertTrue(self.me.check_password(update_data['password']))


class Perm(APITestCase):

    def setUp(self):
        self.url = reverse('user-detail', kwargs={'pk': self.user1.pk})

    @classmethod
    def setUpTestData(cls):
        cls.superuser = User.objects.create_superuser(username='admin', password='1234')
        cls.user1 = User.objects.create_user(username='user1',
                                                      password='1234')
        cls.user2 = User.objects.create_user(username='user2',
                                                      password='1234')

    def test_unauthenticated(self):
        response = self.client.patch(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user(self):
        self.client.force_login(self.user1)
        response = self.client.patch(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_other_user(self):
        self.client.force_login(self.user2)
        response = self.client.patch(self.url)
        self.client.logout()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
