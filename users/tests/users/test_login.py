from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from rest_framework.views import status

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('token_obtain')

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='admin', password='1234')

    def test_data_required(self):
        credentials = {}
        response = self.client.post(self.url, data=credentials)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_successful_login(self):
        credentials = {
            'username': 'admin',
            'password': '1234'
        }
        response = self.client.post(self.url, data=credentials)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_wrong_password(self):
        credentials = {
            'username': 'admin',
            'password': '1235'
        }
        response = self.client.post(self.url, data=credentials)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
