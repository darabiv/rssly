from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from rest_framework.views import status

User = get_user_model()


class Operation(APITestCase):

    def setUp(self):
        self.url = reverse('user-list')

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='admin', password='1234')

    def test_password_required(self):
        signup_data = {
            'username': 'admin2',
            'password': ''
        }
        response = self.client.post(self.url, data=signup_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_successful_signup(self):
        signup_data = {
            'username': 'admin2',
            'password': '!@123456789'
        }
        response = self.client.post(self.url, data=signup_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_existing_username(self):
        signup_data = {
            'username': 'admin',
            'password': 'admin@1234'
        }
        response = self.client.post(self.url, data=signup_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
