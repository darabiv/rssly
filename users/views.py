from django.contrib.auth import get_user_model
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from users.permissions import UserPermissions
from users.serializers import UserSerializer


User = get_user_model()


class UserViewSet(GenericViewSet,
                  mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.CreateModelMixin,
                  mixins.UpdateModelMixin):
    queryset = User.objects.order_by('-date_joined').prefetch_related('groups')
    serializer_class = UserSerializer
    filter_fields = '__all__'
    ordering_fields = '__all__'
    search_fields = ('username', 'email', )
    permission_classes = [UserPermissions]

    def get_queryset(self):
        qs = super().get_queryset()
        user = self.request.user
        if not user.is_superuser:
            qs = qs.filter(is_superuser=False)

        return qs