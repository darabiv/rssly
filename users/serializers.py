from copy import deepcopy

from django.contrib.auth import get_user_model, password_validation
from django.db import transaction
from rest_framework.serializers import ModelSerializer

User = get_user_model()


class UserSerializer(ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('username', 'password', 'first_name', 'last_name', 'email')

    def validate(self, data):
        password = data.get('password')
        if password:
            copy = deepcopy(data)
            copy.pop('groups', None)
            copy.pop('user_permissions', None)
            user = self.instance or User(**copy)
            password_validation.validate_password(password, user=user)

        return data

    def create(self, validated_data):
        instance = User.objects.create_user(**validated_data)
        return instance

    def update(self, instance, validated_data):
        password = validated_data.get('password', None)

        with transaction.atomic():
            instance = super(UserSerializer, self).update(instance,
                                                          validated_data)
            if password:
                instance.set_password(password)

            instance.save()

        return instance
