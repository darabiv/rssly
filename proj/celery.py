import os

from celery import Celery
from django.conf import settings


os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'proj.settings')

app = Celery('rssly')
app.conf.task_default_queue = settings.CELERY_TASK_DEFAULT_QUEUE
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self, a=True):
    print('debug task started')
    print('Request: {0!r}'.format(self.request))
