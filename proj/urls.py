from django.contrib import admin
from django.urls import include, path, re_path
from django.conf.urls.i18n import i18n_patterns
from rest_framework.routers import DefaultRouter

from proj.swagger import schema_view

from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView)

from users.views import UserViewSet
from feeds.views import FeedViewSet, SubscriptionViewSet,\
    FavouriteViewSet


router = DefaultRouter()

router.register(r'users', UserViewSet)
router.register(r'feeds', FeedViewSet)
router.register(r'subscriptions', SubscriptionViewSet)
router.register(r'favourites', FavouriteViewSet)


urlpatterns = i18n_patterns(
    # Admin route
    path('admin/', admin.site.urls),

    # Swagger routes
    re_path(r'^swagger(?P<format>\.json|\.yaml)$',
            schema_view.without_ui(cache_timeout=0),
            name='schema-json'),
    path('swagger/',
         schema_view.with_ui('swagger', cache_timeout=0),
         name='schema-swagger-ui'),
    path('redoc/',
         schema_view.with_ui('redoc', cache_timeout=0),
         name='schema-redoc'),

    # DefaultRouter's routes
    path('api/v1/', include(router.urls)),

    path(
        'api/v1/jwt/create/',
        TokenObtainPairView.as_view(),
        name='token_obtain'
    ),
    path(
        'api/v1/jwt/refresh/',
        TokenRefreshView.as_view(),
        name='token_refresh'
    ),
)
